setup_project_structure <- function(namess=c('out_dir','data_dir','temp_dir'),dirn=c('outputs','data','temp')) {	
	
	dir_list <- function(obj,fname) {
		if(!file.exists(fname)) dir.create(fname)
		assign(obj,fname)
		assign(obj,fname,envir = as.environment(1))
	}
	mapply(dir_list,namess,dirn)
}
