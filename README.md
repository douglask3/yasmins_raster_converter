Regid and convert from one raste format to another
==================================================

Reads in multiple files of the same raster format and converts to a different raster format. Layers are also separated into different files.

Converts too and from rasters that are supported by package raster. Fore more info, type:
		> ?raster
	
How to run
----------
	
To convert raster, first set the cfg lines at the top of the file 'openRegridWrite.r'. Then run:
		> source("openRegridWrite.r")
	
by default, your input raster files should be in the 'data/' dir, whilst output raster are written to 'outputs/' dir.


cfg key
----------

***i/o info***	
Define input raster and output raster through:

- **input_files**   *Vector of filenames to be converted*

- **input_vnmes**   *Variable names. One for each file*

- **input_format**	*File format*

- **otput_format**	*Format of converted file*

***Extent info***	

- **lon_range** 
	*The range of the longitudes for re-gridding. Areas of the raster outside this rage are cropped*

- **lat_range**
	*ditto latitude*	

- **agg_fact**
	*Rescaling factor. How much the file should be aggregated by*
