	
##########################################################################################
## Configuration																		##
##########################################################################################
	## define paths ##
	input_files		<-	c('TC1.nc','TC2.nc')
	input_vnmes		<-	c('TC','TC')
	input_format	<- 	'.nc'
	otput_format	<-	'.asc'
	
	## Define area for regridding ##
	lon_range 		<-	c(112.5,154)
	lat_range		<- 	c(-44,-10.75)
	
	## aggregates cells by a factor of... ##
	agg_fact		<- 5
	

##########################################################################################
## sourcing libs etc																	##
##########################################################################################
	## Source libs ##
	source("libs/source_all_libs.r")
	source_all_libs()
	setup_project_structure()
	install_and_source_library("raster")
	
##########################################################################################
## Gubbins																				##
##########################################################################################
	
	openCropAggWrite <- function(input_file,input_vnme) {
		## constructs input and output file path ##
		c(input_file,out_file):=set_io_files(input_file,input_format)
	
		## opens data ##
		dat=brick(input_file,varname=input_vnme)
	
		## Operation for Croping, aggregating and writing for 1 layer
		cropAggWrite <- function(r) {
			i=bandnr(r) 								# Find band number
			r=crop(r,extent(c(lon_range,lat_range))) 	# crop to pre defined area
			r=aggregate(r,fact=agg_fact) 				# Aggregate
		
			filename=paste(out_file,"-bandnr-",i,otput_format,sep="") # Define output filename 
			writeRaster(r,filename,overwrite=TRUE)						# Write layer to filename
		}
		## Applied function to all layers ##
		layer.apply(dat,cropAggWrite)
	}
	
	mapply(openCropAggWrite,input_files,input_vnmes)
##########################################################################################
## 																						##
##########################################################################################